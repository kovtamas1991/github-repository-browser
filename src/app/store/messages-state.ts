import { Injectable } from '@angular/core';
import { Selector, State, Action, StateContext } from '@ngxs/store';
import { isNullOrUndefined } from 'util';
import { Message } from 'primeng/api/message';
import { MessageService } from 'primeng/api';

/*
    The purpose of this state is to have a layer between the application and
    the message service / interface of PrimeNG. This way this application doesn't
    depend on it so much.
*/


// This interface is created so that I don't use the interface from PrimeNG directly
export interface MessageModel extends Message {

    // Just to make the detail field required
    detail: string;

}

// It is used to configure the component that displays the messages
export interface MessageConfigurationModel {

    position: string;
    defaultClosable: boolean;
    defaultSticky: boolean;

}

export interface MessageStateModel {

    configurations: MessageConfigurationModel;

}

const typeName = `[MESSAGES_STATE]`;

export class SetConfigurations {
    public static readonly type = `${typeName} Set configurations`;
    constructor(public readonly payload: MessageConfigurationModel) { }
}

export class AddMessage {
    public static readonly type = `${typeName} Add success message`;
    constructor(public readonly payload: MessageModel) { }
}


const defaultState: MessageStateModel = {
    configurations: {
        position: 'top-right',
        defaultClosable: true,
        defaultSticky: true
    }
};

@State<MessageStateModel>({
    name: 'messagesState',
    defaults: defaultState
})
@Injectable()
export class MessagesState {

    @Selector()
    public static position(state: MessageStateModel): string {
        return state.configurations.position;
    }
    @Selector()
    public static closable(state: MessageStateModel): boolean {
        return state.configurations.defaultClosable;
    }
    @Selector()
    public static sticky(state: MessageStateModel): boolean {
        return state.configurations.defaultSticky;
    }

    constructor(private readonly messageService: MessageService) {
    }

    @Action(SetConfigurations)
    public setConfigurations(ctx: StateContext<MessageStateModel>, action: SetConfigurations): void {
        ctx.setState({ configurations: action.payload });
    }

    @Action(AddMessage)
    public addMessage(ctx: StateContext<MessageStateModel>, action: AddMessage): void {
        const state = ctx.getState();
        const defaultClosable = state.configurations.defaultClosable;
        const defaultSticky = state.configurations.defaultSticky;

        const message = { ...action.payload };
        message.closable = isNullOrUndefined(message.closable) ? defaultClosable : message.closable;
        message.sticky = isNullOrUndefined(message.sticky) ? defaultSticky : message.sticky;

        this.messageService.add(message);
    }

}
