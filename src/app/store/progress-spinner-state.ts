import { State, Selector, StateContext, Action } from '@ngxs/store';

export interface ProgressSpinnerModel {

    isSpinnerOn: boolean;
    activeRequestCount: number;

}

const typeName = '[PROGRESS_SPINNER]';

export class RequestSpinnerStart {
    public static readonly type = `${typeName} Request spinner start`;
}

export class RequestSpinnerStop {
    public static readonly type = `${typeName} Request spinner stop`;
}

@State<ProgressSpinnerModel>({
    name: 'progressSpinner',
    defaults: {
        isSpinnerOn: false,
        activeRequestCount: 0
    }
})
export class ProgressSpinnerState {

    @Selector()
    public static isSpinnerOn(state: ProgressSpinnerModel): boolean {
        return state.isSpinnerOn;
    }

    @Action(RequestSpinnerStart)
    public requestSpinnerStart(ctx: StateContext<ProgressSpinnerModel>): void {
        const state = ctx.getState();
        ctx.setState({
            isSpinnerOn: true,
            activeRequestCount: state.activeRequestCount + 1
        });
    }

    @Action(RequestSpinnerStop)
    public requestSpinnerStop(ctx: StateContext<ProgressSpinnerModel>): void {
        const state = ctx.getState();
        const activeRequestCount = Math.max(0, state.activeRequestCount - 1);
        const isSpinnerOn = activeRequestCount > 0;
        ctx.setState({
            isSpinnerOn,
            activeRequestCount
        });
    }

}
