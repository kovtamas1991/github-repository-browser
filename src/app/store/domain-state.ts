import { State, Selector, StateContext, Action } from '@ngxs/store';

export interface DomainModel {
    domain: string;
}

export class SetDomain {
    public static readonly type: string = '[DOMAIN] setDomain';
    constructor(public readonly payload: string) {}
}

@State<DomainModel>({
    name: 'domain',
    defaults: {
        domain: 'https://api.github.com'
    }
})
export class DomainState {

    @Selector()
    public static domain(state: DomainModel): any {
        return state.domain;
    }

    @Action(SetDomain)
    public setDomain(ctx: StateContext<DomainModel>, action: SetDomain): any {
        ctx.setState({
            domain: action.payload
        });
    }

}
