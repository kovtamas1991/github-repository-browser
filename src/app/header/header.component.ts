import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    public menuItems: MenuItem[];

    public ngOnInit(): void {
        this.menuItems = [
            {label: 'Page 1', routerLink: '/page1'},
            {label: 'Page 2', routerLink: '/page2'}
        ];
    }

}
