import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError } from 'rxjs/internal/operators/catchError';
import { filter } from 'rxjs/internal/operators/filter';
import { tap } from 'rxjs/internal/operators/tap';
import { AddMessage, MessageModel } from '../store/messages-state';

/**
 * This interceptor catches every error received through an http call.
 * It prints the error message as toast message and rethrows the error so that the
 * "success callback" of the caller won't get executed.
 */
@Injectable()
export class ErrorHandlerInterceptorService implements HttpInterceptor {

    constructor(private readonly store: Store) { }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            filter(response => response instanceof HttpResponseBase),
            tap((response: any) => {
                const responseBase = response as HttpResponseBase;
                if (!responseBase.ok) {
                    this.handleError(responseBase);
                }

            }),
            catchError(response => {
                this.handleError(response);
                return throwError(response);
            })
        );
    }

    private handleError(response: HttpResponseBase): void {
        const errorSummary = response.statusText;
        let errorMessage = null;
        if (response instanceof HttpErrorResponse) {
            errorMessage = (response as HttpErrorResponse).error.detail;
        }
        const message: MessageModel = { detail: errorMessage, summary: errorSummary, severity: 'error' };
        this.store.dispatch(new AddMessage(message));
    }

}
