import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError, tap } from 'rxjs/operators';
import { RequestSpinnerStart, RequestSpinnerStop } from '../store/progress-spinner-state';

/**
 * This interceptor is responsible for automatically starting the
 * progress spinner when an http call is fired. It also stops the spinner
 * when the call finishes. The spinner will only disappear when there is no
 * http call in progress.
 */
@Injectable()
export class ProgressSpinnerInterceptorService implements HttpInterceptor {

    constructor(private readonly store: Store) { }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.store.dispatch(new RequestSpinnerStart());
        return next.handle(req).pipe(
            tap((httpEvent) => {
                // With this condition we ignore the response from the OPTIONS requests
                if (httpEvent instanceof HttpResponseBase) {
                    this.store.dispatch(new RequestSpinnerStop());
                }
            }),
            catchError(response => {
                this.store.dispatch(new RequestSpinnerStop());
                return throwError(response);
            })
        );
    }

}
