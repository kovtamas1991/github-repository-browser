import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Store } from '@ngxs/store';
import { DomainState } from '../store/domain-state';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

/**
 * This interceptor is created mainly for maintainability and convenience.
 * If an http request starts with the / character, than this interceptor
 * will prepend it with the domain. This way the services will only need to
 * use the relative part of the urls and the domain will only be declared
 * in one part of the application.
 */
@Injectable()
export class DomainInterceptorService implements HttpInterceptor {


    constructor(private readonly store: Store) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.url.startsWith('/')) {
            return next.handle(req);
        }

        const domain = this.store.selectSnapshot(DomainState.domain);
        const newUrl = `${domain}${req.url}`;
        const newReq = req.clone({ url: newUrl });

        return next.handle(newReq);
    }

}
