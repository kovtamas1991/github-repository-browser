import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MenuModule } from 'primeng/menu';
import { ButtonModule } from 'primeng/button';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';


import { HeaderComponent } from './header/header.component';
import { DomainInterceptorService } from './interceptors/domain-interceptor-service';
import { DomainState } from './store/domain-state';
import { ProgressSpinnerState } from './store/progress-spinner-state';
import { ProgressSpinnerInterceptorService } from './interceptors/progress-spinner-interceptor-service';
import { Page1Component } from './placeholder-pages/page1/page1.component';
import { Page2Component } from './placeholder-pages/page2/page2.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ErrorHandlerInterceptorService } from './interceptors/error-handler-interceptor-service';
import { MessagesState } from './store/messages-state';
import { environment } from 'src/environments/environment';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        Page1Component,
        Page2Component,
        ErrorPageComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgxsModule.forRoot([DomainState, ProgressSpinnerState, MessagesState], {
            developmentMode: !environment.production
        }),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        BrowserAnimationsModule,
        HttpClientModule,
        MenuModule,
        ButtonModule,
        ProgressSpinnerModule,
        ToastModule
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: HTTP_INTERCEPTORS, useClass: DomainInterceptorService, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ProgressSpinnerInterceptorService, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptorService, multi: true },
        MessageService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
