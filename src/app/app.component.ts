import { Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { MessagesState } from './store/messages-state';
import { ProgressSpinnerState } from './store/progress-spinner-state';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    // Spinner visibility
    @Select(ProgressSpinnerState.isSpinnerOn)
    public isSpinnerOn$: Observable<boolean>;

    // Message configuration
    @Select(MessagesState.position)
    public messagePosition$: Observable<string>;

    public title = 'github-repository-browser';

}
