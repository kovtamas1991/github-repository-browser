import { isNullOrUndefined } from 'util';
import { getMetadataField, getMetadataFieldNameFor, ValidationMetadata } from './validation/meta';
import { validateNotEmpty, validateRequired, notEmptyErrorMessageFn, requiredErrorMessageFn, validateNotNegative, notNegativeErrorMessageFn } from './validation/validators';

export function ValidateArgs(target: any, key: string, descriptor: PropertyDescriptor): void {
    const metadataFieldName = getMetadataFieldNameFor(key);
    const providedValidationMetadataList = target[metadataFieldName] as ValidationMetadata[];
    if (isNullOrUndefined(providedValidationMetadataList) || providedValidationMetadataList.length === 0) {
        return;
    }

    const originalMethod = descriptor.value;
    const errorMessages = [];
    descriptor.value = function(): any {
        const originalArguments = arguments;
        providedValidationMetadataList.forEach(currentMetadata => {
            const argValue = originalArguments[currentMetadata.index];
            const isValid = currentMetadata.validate(argValue);
            if (!isValid) {
                errorMessages.push(currentMetadata.errorMessageFn(key, currentMetadata.index, argValue));
            }
        });

        if (errorMessages.length > 0) {
            throw new Error(`${errorMessages.reverse().join('')}\n`);
        }

        return originalMethod.apply(this, arguments);
    };

}

function AllArgValidation(descriptor: PropertyDescriptor,
                          methodName: string,
                          validate: (argValue: any) => boolean,
                          errorMessageFn: (methodName: string, argIndex: number, argValue: any) => string): void {
    const originalMethod = descriptor.value;
    descriptor.value = function(): any {
        const argsArray = Array.from(arguments);
        for (let i = 0; i < argsArray.length; i++) {
            const currentArg = argsArray[i];
            if (!validate(currentArg)) {
                throw new Error(errorMessageFn(methodName, i, currentArg));
            }
        }

        return originalMethod.apply(this, arguments);
    };
}

export function NotEmpty(target: any, key: string, index: number): void {
    const validationMetaArray = getMetadataField(target, key);
    validationMetaArray.push({ index, validate: validateNotEmpty, errorMessageFn: notEmptyErrorMessageFn });
}

export function Required(target: any, key: string, index: number): void {
    const validationMetaArray = getMetadataField(target, key);
    validationMetaArray.push({ index, validate: validateRequired, errorMessageFn: requiredErrorMessageFn });
}

export function NotNegative(target: any, key: string, index: number): void {
    const validationMetaArray = getMetadataField(target, key);
    validationMetaArray.push({ index, validate: validateNotNegative, errorMessageFn: notNegativeErrorMessageFn });
}

export function AllArgsRequired(target: any, key: string, descriptor: PropertyDescriptor): void {
    AllArgValidation(descriptor, key, validateRequired, requiredErrorMessageFn);
}

export function AllArgsNotEmpty(target: any, key: string, descriptor: PropertyDescriptor): void {
    AllArgValidation(descriptor, key, validateNotEmpty, notEmptyErrorMessageFn);
}
