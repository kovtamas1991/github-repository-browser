import { isNullOrUndefined, isNumber } from 'util';

export function validateRequired(value: any): boolean {
    return !isNullOrUndefined(value);
}
export function requiredErrorMessageFn(methodName: string, argIndex: number, argValue: any): string {
    return `The ${methodName} method's ${argIndex}th parameter is required, but it was called with the value "${argValue}"!`;
}


export function validateNotEmpty(value: any): boolean {
    return !isNullOrUndefined(value) && !isNullOrUndefined(value.length) && value.length > 0;
}
export function notEmptyErrorMessageFn(methodName: string, argIndex: number, argValue: any): string {
    return `The ${methodName} method's ${argIndex}th parameter must be "NotEmpty" but it was called with the value '${JSON.stringify(argValue)}'!`;
}

export function validateNotNegative(value: any): boolean {
    return !isNullOrUndefined(value) && isNumber(value) && value >= 0;
}
export function notNegativeErrorMessageFn(methodName: string, argIndex: number, argValue: any): string {
    return `The ${methodName} method's ${argIndex}th parameter must be a non-negative number, but it was called with the value "${argValue}"!`;
}
