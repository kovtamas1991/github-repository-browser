export interface ValidationMetadata {
    index: number;
    validate: (argValue: any) => boolean;
    errorMessageFn: (methodName: string, argIndex: number, argValue: any) => string;
}

export function getMetadataFieldNameFor(methodName: string): string {
    return `_validation_argument_indices_for_${methodName}`;
}

export function getMetadataField(target: any, key: string): ValidationMetadata[] {
    const metadataFieldName = getMetadataFieldNameFor(key);
    if (!target[metadataFieldName]) {
        target[metadataFieldName] = [];
    }

    return target[metadataFieldName];
}

