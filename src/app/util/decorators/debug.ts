import { isObject, isString, isPrimitive, isArray, isNullOrUndefined } from 'util';

function traceLog(msg: string, payload?: any): void {
    const style = 'color: gray; font-weight: bold;';
    const indentAmount = 3;
    styledLog(msg, style, payload, indentAmount);
}

function changeLog(msg: string, payload?: any): void {
    const  style = 'color: green; font-weight: bold;';
    styledLog(msg, style, payload, 0);
}

function styledLog(msg: string, style: string, payload?: any, payloadIndent?: number): void {
    if (isNullOrUndefined(payload)) {
        console.log(`%c${msg}`, style);
    } else {
        let indentStr = '';
        for (let i = 0; i < payloadIndent; i++) {
            indentStr += ' ';
        }
        console.log(`%c${msg}\n${indentStr}`, style, payload);
    }

}

export function TraceLog(target: any, key: string, descriptor: PropertyDescriptor): void {

    const originalMethod = descriptor.value;
    descriptor.value = function(): any {

        const argsArray = Array.from(arguments);
        const hasObjectArg = argsArray.some(arg => isObject(arg));

        if (hasObjectArg) {
            traceLog(`>>> ${key}(...)`);
            traceLog(`    arguments:`, argsArray);
        } else {
            const argsString = argsArray
                .map(arg => isString(arg) ? `'${arg}'` : arg)
                .join(', ');
            traceLog(`>>> ${key}(${argsString})`);
        }

        const result = originalMethod.apply(this, arguments);
        traceLog(`<<< ${key} -------`);

        return result;
    };

}

export function LogChanges(target: object, key: string): void {

    // original value
    let value = target[key];

    // getter and setter
    const getter = () => value;
    const setter = (newValue: any) => {
        value = newValue;
        logChange(key, newValue);
        if (isArray(newValue)) {
            setUpArrayForLogging(key, newValue);
        }
    };

    // replace property with the getter and setter
    Object.defineProperty(target, key, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true
    });

}

function setUpArrayForLogging(key: string, array: any[]): void {
    const modifyerMethodNames = ['push', 'unshift', 'pop', 'fill', 'reverse', 'shift'];
    modifyerMethodNames.forEach(methodName => {
        const originalMethod = array[methodName];
        array[methodName] = function(): any {
            const result = originalMethod.apply(this, arguments);
            logChange(key, this);
            return result;
        };
    });

}

function logChange(key: string, newValue: any): void {
    if (isPrimitive(newValue)) {
        changeLog(`A(z) ${key} új éréke: ${newValue}`);
    } else {
        const indentAmount = 2;
        changeLog(`A(z) ${key} új értéke:\n`, JSON.stringify(newValue, null, indentAmount));
    }
}
