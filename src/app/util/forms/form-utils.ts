import { ValidationErrors, FormGroup, AbstractControl, FormArray } from '@angular/forms';
import { AllArgsRequired, ValidateArgs, Required, NotEmpty } from '../decorators/method-param-validation';
import { isNullOrUndefined } from 'util';

/**
 * A collection of utility methods that can be helpful
 * when dealing with reactive forms.
 */
export class FormUtils {

    @AllArgsRequired
    public static markFormAsDirty(form: FormGroup | FormArray): void {
        Object.keys(form.controls).forEach(controlName => {
            const control = form.controls[controlName];
            const isGroup = control instanceof FormGroup;
            if (isGroup) {
                FormUtils.markFormAsDirty(control as FormGroup);
            } else {
                control.markAsDirty();
            }
        });
    }

    @AllArgsRequired
    public static getAllErrorNames(form: FormGroup): string[] {
        let errors = isNullOrUndefined(form.errors) ? [] : Object.keys(form.errors);
        Object.keys(form.controls).forEach(controlName => {
            const control = form.controls[controlName];
            const isGroup = control instanceof FormGroup;
            if (isGroup) {
                errors = errors.concat(FormUtils.getAllErrorNames(control as FormGroup));
            }
            const currentErrorNames = isNullOrUndefined(control.errors) ? [] : Object.keys(control.errors);
            errors = errors.concat(currentErrorNames);
        });
        return errors;
    }

    @ValidateArgs
    public static addErrorsIfNotPresent(@Required control: AbstractControl, errors: ValidationErrors): boolean {
        if (isNullOrUndefined(errors)) {
            return false;
        }

        if (isNullOrUndefined(control.errors)) {
            control.setErrors(FormUtils.copyErrors(errors));
            return false;
        }

        const presentErrors = Object.keys(control.errors);
        const givenErrors = Object.keys(errors);
        const newErrorNames = givenErrors.filter(errorName => !presentErrors.includes(errorName));
        if (newErrorNames.length === 0) {
            return false;

        }
        const newErrorsObject = {};
        newErrorNames.forEach(errorName => {
            newErrorsObject[errorName] = errors[errorName];
        });

        const mergedErrors = { ...control.errors, ...newErrorsObject };
        control.setErrors(mergedErrors);
        return true;
    }

    public static mergeErrors(err1: ValidationErrors, err2: ValidationErrors): ValidationErrors {
        if (err1 === null && err2 === null) {
            return null;
        }

        return { ...err1, ...err2 };
    }

    // Copy errors so that the separate controls don't share the same error object
    public static copyErrors(error: ValidationErrors | null): ValidationErrors | null {
        return error === null ? null : { ...error };
    }

    @ValidateArgs
    public static removeError(errors: ValidationErrors, @NotEmpty errorName: string): ValidationErrors | null {
        if (isNullOrUndefined(errors)) {
            return null;
        }
        const remainingErrors = {...errors};
        delete remainingErrors[errorName];
        const stillHasErrors = Object.keys(remainingErrors).length > 0;
        return stillHasErrors ? remainingErrors : null;
    }

    @ValidateArgs
    public static addErrors(errors: ValidationErrors, @Required newErrors: ValidationErrors): ValidationErrors {
        const finalErrors = isNullOrUndefined(errors) ? {} : {...errors};
        return {...finalErrors, ...newErrors};
    }

}
