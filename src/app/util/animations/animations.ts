import { trigger, state, style, transition, animate } from '@angular/animations';

export const GROW_IN_SHRINK_OUT =
    trigger('growInShrinkOut', [
        state('void', style({
            height: 0,
            opacity: 0,
            overflow: 'hidden'
        })),
        transition('void <=> *', animate('150ms'))
    ]);
