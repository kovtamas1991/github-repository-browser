import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryRootComponent } from './history-root/history-root.component';


const routes: Routes = [
    {
        path: '',
        component: HistoryRootComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HistoryRoutingModule { }
