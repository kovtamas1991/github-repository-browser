import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryRoutingModule } from './history-routing.module';
import { HistoryRootComponent } from './history-root/history-root.component';
import { NgxsModule } from '@ngxs/store';


@NgModule({
  declarations: [HistoryRootComponent],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    NgxsModule.forFeature([])
  ]
})
export class HistoryModule { }
