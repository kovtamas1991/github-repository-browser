import { ControlValueAccessor } from '@angular/forms';
import { OperatorModel } from 'src/app/search/models/values-with-operator/operator-model';
import { isNullOrUndefined } from 'util';

/**
 * A base class that provides a few properties and methods that are common
 * between the components that represent the value part of the
 * values with operator concept.
 */
export abstract class TypedValuesBase implements ControlValueAccessor {

    public hasSelectedOperator: boolean;
    public isBinaryOperator: boolean;      // So far only the BETWEEN operator is binary
    protected _selectedOperator: OperatorModel;

    protected ngModelChange: (values: number[]) => void;

    public writeValue(obj: any): void {
        // The ngModelChange part is included so that we can ignore the initial write value call
        if (!isNullOrUndefined(this.ngModelChange) && isNullOrUndefined(obj)) {
            this.onFormReset();
        }
    }

    public registerOnChange(fn: any): void {
        this.ngModelChange = fn;
    }
    public registerOnTouched(fn: any): void {
    }

    protected abstract onFormReset(): void;


}
