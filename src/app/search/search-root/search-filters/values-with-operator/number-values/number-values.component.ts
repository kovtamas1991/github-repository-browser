import { Component, forwardRef, Input } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator, Validators } from '@angular/forms';
import { merge } from 'rxjs/internal/observable/merge';
import { distinctUntilChanged } from 'rxjs/operators';
import { OperatorModel } from 'src/app/search/models/values-with-operator/operator-model';
import { minMaxValidator } from 'src/app/search/validators/search-filter-validators';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { isNullOrUndefined } from 'util';
import { TypedValuesBase } from '../typed-values-base/typed-values-base';

/**
 * This component is a form control, emits null when invalid or partially filled.
 * Represents the 'number' value type of the values with operator concept.
 * This control has a default value, but only when the BETWEEN operator is selected.
 */
@Component({
    selector: 'app-number-values',
    templateUrl: './number-values.component.html',
    styleUrls: ['./number-values.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NumberValuesComponent), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => NumberValuesComponent), multi: true }
    ]
})
export class NumberValuesComponent extends TypedValuesBase implements Validator {

    // When the operator is BETWEEN, these are the default min and max values for the range picker
    private readonly defaultMinValue = 0;
    private readonly defaultMaxValue = 100;

    public unaryFormGroup: FormGroup;
    public binaryFormGroup: FormGroup;

    @Input()
    public set selectedOperator(operator: OperatorModel) {
        this._selectedOperator = operator;
        this.hasSelectedOperator = !isNullOrUndefined(this._selectedOperator);
        this.isBinaryOperator = this._selectedOperator?.id === 'BETWEEN';

        if (this.ngModelChange) {
            setTimeout(() => {
                this.emitValues();
            }, 0);
        }
    }
    public get selectedOperator(): OperatorModel {
        return this._selectedOperator;
    }

    constructor(private readonly fb: FormBuilder) {
        super();
        this._selectedOperator = null;
        this.hasSelectedOperator = false;
        this.isBinaryOperator = false;
        this.initFormGroups();
        this.setUpResetValues();
        this.emitValuesOnChange();
    }

    private initFormGroups(): void {
        this.unaryFormGroup = this.fb.group({
            singleValue: [null]
        });
        this.binaryFormGroup = this.fb.group({
            min: this.fb.control(this.defaultMinValue, { validators: [Validators.min(0), Validators.required], updateOn: 'blur' }),
            max: this.fb.control(this.defaultMaxValue, { validators: [Validators.required], updateOn: 'blur' }),
            values: [[this.defaultMinValue, this.defaultMaxValue]]
        }, { validators: minMaxValidator });
    }

    protected onFormReset(): void {
        this.unaryFormGroup.reset();
        this.binaryFormGroup.patchValue({
            min: this.defaultMinValue,
            max: this.defaultMaxValue,
            values: [this.defaultMinValue, this.defaultMaxValue]
        });
        this.ngModelChange(null);
    }

    // This validation only delegates the errors of the active form group
    validate(control: AbstractControl): ValidationErrors {
        const unaryErrors = this.unaryFormGroup.errors;
        const binaryErrors = this.binaryFormGroup.errors;
        return this.isBinaryOperator ? binaryErrors : unaryErrors;
    }

    // When the min and max values are changed and valid, the range picker's value is reset to avoid complications
    private setUpResetValues(): void {
        merge(
            this.binaryFormGroup.controls['min'].valueChanges,
            this.binaryFormGroup.controls['max'].valueChanges
        ).subscribe(this.resetValues.bind(this));

        this.binaryFormGroup.statusChanges
            .pipe(distinctUntilChanged())
            .subscribe(this.resetValues.bind(this));
    }

    private resetValues(): void {
        const minValue = this.binaryFormGroup.controls['min'].value;
        const maxValue = this.binaryFormGroup.controls['max'].value;
        if (!isNullOrUndefined(minValue) && !isNullOrUndefined(maxValue)) {
            this.binaryFormGroup.patchValue({ values: [minValue, maxValue] });
        }
    }

    private emitValuesOnChange(): void {
        merge(this.unaryFormGroup.valueChanges, this.binaryFormGroup.valueChanges)
            .subscribe(this.emitValues.bind(this));
    }

    private emitValues(): void {
        const values = this.getCurrentValues();
        if (!this.isFormValid() || !this.hasAllValues(values)) {
            this.ngModelChange(null);
            return;
        }

        this.ngModelChange(values);
    }

    private isFormValid(): boolean {
        return (this.isBinaryOperator && this.binaryFormGroup.valid)
            || (!this.isBinaryOperator && this.unaryFormGroup.valid);
    }

    private getCurrentValues(): number[] {
        return this.isBinaryOperator
            ? this.binaryFormGroup.controls['values'].value
            : [this.unaryFormGroup.controls['singleValue'].value];
    }

    private hasAllValues(values: number[]): boolean {
        return validateNotEmpty(values) && values.every(val => !isNullOrUndefined(val));
    }


}
