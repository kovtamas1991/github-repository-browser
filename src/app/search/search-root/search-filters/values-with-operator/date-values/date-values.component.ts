import { Component, Input, forwardRef, OnInit } from '@angular/core';
import { TypedValuesBase } from '../typed-values-base/typed-values-base';
import { FormBuilder, FormGroup, AbstractControl, FormArray, NG_VALUE_ACCESSOR, ValidationErrors, Validator, NG_VALIDATORS } from '@angular/forms';
import { OperatorModel } from 'src/app/search/models/values-with-operator/operator-model';
import { isNullOrUndefined } from 'util';
import { FormUtils } from 'src/app/util/forms/form-utils';
import { validateFromTo } from 'src/app/search/validators/search-filter-validators';

/**
 * This component is a form control, emits null when invalid or partially filled.
 * Represents the 'date' value type of the values with operator concept.
 */
@Component({
    selector: 'app-date-values',
    templateUrl: './date-values.component.html',
    styleUrls: ['./date-values.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DateValuesComponent), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => DateValuesComponent), multi: true }
    ]
})
export class DateValuesComponent extends TypedValuesBase implements OnInit, Validator {

    public formGroup: FormGroup;
    public yearRange: string;

    constructor(private readonly fb: FormBuilder) {
        super();
        this.initYearRange();
        this.initFormGroups();
    }

    @Input()
    public set selectedOperator(operator: OperatorModel) {
        this.adjustFormToSelectedOperator(this._selectedOperator, operator);
        this._selectedOperator = operator;
        this.hasSelectedOperator = !isNullOrUndefined(this._selectedOperator);
        this.isBinaryOperator = this._selectedOperator?.id === 'BETWEEN';
        this.formGroup.reset();
    }

    public get selectedOperator(): OperatorModel {
        return this._selectedOperator;
    }

    private initFormGroups(): void {
        this.formGroup = this.fb.group({
            values: this.fb.array([this.fb.control(null)], { validators: validateFromTo })
        });
    }

    public ngOnInit(): void {
        this.emitValuesOnChange();
    }

    private initYearRange(): void {
        const firstGithubRepoYear = '2007';
        const thisYear = (new Date()).getFullYear();
        this.yearRange = `${firstGithubRepoYear}:${thisYear}`;
    }

    // This validation only delegates the errors of the controls that this form has
    public validate(control: AbstractControl): ValidationErrors {
        let errors = null;
        Object.keys(this.formGroup.controls).forEach(controlName => {
            errors = FormUtils.mergeErrors(errors, this.formGroup.controls[controlName].errors);
        });
        return errors;
    }

    private emitValuesOnChange(): void {
        this.formGroup.controls['values'].valueChanges.subscribe(values => {
            const hasEmptyValue = values.some(val => isNullOrUndefined(val));
            const valueToEmit = hasEmptyValue ? null : values;
            this.ngModelChange(valueToEmit);
        });
    }

    protected onFormReset(): void {
        this.formGroup.reset();
        this.ngModelChange(null);
    }

    // The index can only be 0 or 1, as there can only be 1 or 2 controls
    public getLabelForControl(index: number): string {
        if (index === 1) {
            return 'To:';
        }
        return this.isBinaryOperator ? 'From:' : 'Date';
    }

    // The BETWEEN operator needs two values, while the other only require one
    private adjustFormToSelectedOperator(prevOperator: OperatorModel, newOperator: OperatorModel): void {
        if (isNullOrUndefined(newOperator)) {
            return;
        }

        const prevWasBetween = prevOperator?.id === 'BETWEEN';
        const newIsBetween = newOperator?.id === 'BETWEEN';
        if (!prevWasBetween && !newIsBetween) {
            return;
        }

        const valuesFormArray = this.formGroup.controls['values'] as FormArray;
        if (newOperator.id === 'BETWEEN') {
            valuesFormArray.push(this.fb.control(null));
        } else {
            valuesFormArray.removeAt(1);
        }
    }

    // Html helper method
    public get valueControls(): AbstractControl[] {
        const valuesFormArray = this.formGroup.controls['values'] as FormArray;
        return valuesFormArray.controls;
    }


}
