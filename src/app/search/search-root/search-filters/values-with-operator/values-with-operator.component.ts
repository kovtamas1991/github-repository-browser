import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Store } from '@ngxs/store';
import { OperatorModel } from 'src/app/search/models/values-with-operator/operator-model';
import { ValuesWithOperatorModel } from 'src/app/search/models/values-with-operator/values-with-operator-model';
import { OperatorsState } from 'src/app/search/store/operators-state';
import { GROW_IN_SHRINK_OUT } from 'src/app/util/animations/animations';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { isNullOrUndefined } from 'util';
import { FormUtils } from 'src/app/util/forms/form-utils';

/**
 * This component is a form control that connects the comparing operators and values
 * to compare to. When invalid or partially filled, it emits a null value.
 */
@Component({
    selector: 'app-values-with-operator',
    templateUrl: './values-with-operator.component.html',
    styleUrls: ['./values-with-operator.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ValuesWithOperatorComponent), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValuesWithOperatorComponent), multi: true }
    ],
    animations: [GROW_IN_SHRINK_OUT]
})
export class ValuesWithOperatorComponent implements OnInit, ControlValueAccessor, Validator {

    @Input() public type: string;
    @Input() public title: string;
    @Input() public icon?: string;

    public operators: OperatorModel[];
    public formGroup: FormGroup;

    public ngModelChange: (value: ValuesWithOperatorModel<Date | number>) => void;

    constructor(private readonly store: Store, private readonly fb: FormBuilder) {
    }

    public ngOnInit(): void {
        if (!validateNotEmpty(this.type) || !validateNotEmpty(this.title)) {
            throw new Error('The type and title inputs must be provided for the app-values-with-operator component!');
        }
        this.getOperatorsForCurrentType();
        this.initFormGroup();
    }

    private getOperatorsForCurrentType(): void {
        const selector = OperatorsState.getSelectorForType(this.type);
        this.operators = this.store.selectSnapshot(selector);
    }

    private initFormGroup(): void {
        this.formGroup = this.fb.group({
            operator: this.fb.control(this.operators[0], Validators.required),
            values: [null]
        });
    }

    // This validation only delegates the errors of the controls that this form has
    validate(control: AbstractControl): ValidationErrors | null {
        const errors = FormUtils.mergeErrors(
            this.formGroup.controls['operator'].errors,
            this.formGroup.controls['values'].errors
        );
        return errors;
    }

    private setUpNgModelChange(): void {
        this.formGroup.valueChanges
            .subscribe(formValue => {
                const hasValue = !isNullOrUndefined(formValue.values);
                const valueToEmit = hasValue ? formValue : null;
                this.ngModelChange(valueToEmit);
            });
    }

    public get hasIcon(): boolean {
        return !isNullOrUndefined(this.icon);
    }

    // Control value accessor methods ---------------
    public writeValue(newValue: ValuesWithOperatorModel<Date | number>): void {
        if (isNullOrUndefined(newValue)) {
            this.formGroup.reset();
            this.formGroup.patchValue({ operator: this.operators[0] });
        } else {
            this.formGroup.patchValue(newValue);
        }
    }

    public registerOnChange(fn: any): void {
        this.ngModelChange = fn;
        this.setUpNgModelChange();
    }

    public registerOnTouched(fn: any): void { }

}
