import { Component, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { Subscription } from 'rxjs/internal/Subscription';
import { AddMessage } from 'src/app/store/messages-state';
import { GROW_IN_SHRINK_OUT } from 'src/app/util/animations/animations';
import { FormUtils } from 'src/app/util/forms/form-utils';
import { SearchFiltersModel } from '../../models/filter/search-filters-model';
import { SearchConfigurationState, ToggleAdvancedMode } from '../../store/search-configuration-state';
import { InitiateSearch } from '../../store/search-result-options/search-result-options-actions';
import { ResetResults } from '../../store/search-result-options/search-result-state';
import { hasSearchInForSearchText, requireFilterValue } from '../../validators/search-filter-validators';
import { getHighestPriorityErrorMessage } from '../../validators/validation-errors';


@Component({
    selector: 'app-search-filters',
    templateUrl: './search-filters.component.html',
    styleUrls: ['./search-filters.component.scss'],
    animations: [GROW_IN_SHRINK_OUT]
})
export class SearchFiltersComponent implements OnDestroy {

    // Stores all the state.select(...) subscriptions so that when the components gets destroyed, I can unsubscribe
    private readonly manualStoreSubscriptions: Subscription[];

    public mainFormGroup: FormGroup;
    public basicSearchGroup: FormGroup;
    public advancedSearchGroup: FormGroup;

    @Select(SearchConfigurationState.toggleIcon)
    public toggleSearchModeIcon$: Observable<string>;

    @Select(SearchConfigurationState.isAdvanced)
    public isInAdvancedMode$: Observable<boolean>;

    constructor(private readonly store: Store, private readonly fb: FormBuilder) {
        this.manualStoreSubscriptions = [];
        this.initFormGroups();
        this.listenToSearchModeChange();
    }

    private listenToSearchModeChange(): void {
        const subscription = this.store.select(SearchConfigurationState.isAdvanced).subscribe((isAdvanced: boolean) => {
            if (isAdvanced) {
                this.toAdvancedSearchMode();
            } else {
                this.toSimpleSearchMode();
            }
        });
        this.manualStoreSubscriptions.push(subscription);
    }

    private initFormGroups(): void {
        // The advanced search group is only prepared here, but not added until it is toggled
        this.advancedSearchGroup = this.fb.group({
            username: [null, Validators.minLength(3)],
            languages: [null],
            topics: [null],
            organization: [null, Validators.minLength(3)],
            stars: this.fb.control(null, { updateOn: 'change' }),
            created: this.fb.control(null, { updateOn: 'change' }),
            size: this.fb.control(null, { updateOn: 'change' })
        });

        // Only put into a separate field to make accessing it easier in the html
        this.basicSearchGroup = this.fb.group({
            searchText: this.fb.control(null, { validators: [Validators.required, Validators.minLength(3)] }),
            searchIn: this.fb.group({
                name: [true],
                description: [false],
                readme: [false]
            })
        }, { validators: [hasSearchInForSearchText] });

        this.mainFormGroup = this.fb.group({
            basicSearchFilters: this.basicSearchGroup
        }, { validators: requireFilterValue, updateOn: 'blur' });
    }

    public test(control: AbstractControl): string {
        return getHighestPriorityErrorMessage(control.errors);
    }

    // Html helper method, because the formControlName doesn't work with the PrimeNG's checkbox component
    public getCheckboxControl(controlName: string): AbstractControl {
        const searchByControl = this.mainFormGroup.controls['basicSearchFilters'] as FormGroup;
        const searchInControl = searchByControl.controls['searchIn'] as FormGroup;
        return searchInControl.controls[controlName];
    }

    public toggleAdvancedSearch(): void {
        this.store.dispatch(new ToggleAdvancedMode());
    }

    private toAdvancedSearchMode(): void {
        this.mainFormGroup.addControl('advancedSearchFilters', this.advancedSearchGroup);
        const searchByGroup = this.mainFormGroup.controls['basicSearchFilters'] as FormGroup;
        searchByGroup.controls['searchText'].setValidators([Validators.minLength(3)]);
        const errors = FormUtils.removeError(searchByGroup.controls['searchText'].errors, 'required');
        searchByGroup.controls['searchText'].setErrors(errors);
    }

    private toSimpleSearchMode(): void {
        this.mainFormGroup.removeControl('advancedSearchFilters');
        const searchByGroup = this.mainFormGroup.controls['basicSearchFilters'] as FormGroup;
        searchByGroup.controls['searchText'].setValidators([Validators.required, Validators.minLength(3)]);
        searchByGroup.controls['searchText'].updateValueAndValidity();
    }

    public reset(): void {
        this.resetForm();
        this.store.dispatch(new ResetResults());
    }

    private resetForm(): void {
        this.mainFormGroup.reset();
        const isAdvanced = this.store.selectSnapshot(SearchConfigurationState.isAdvanced);
        if (!isAdvanced) {
            this.advancedSearchGroup.reset();
        }
        this.mainFormGroup.patchValue({
            basicSearchFilters: {
                // The searchIn is the only part that has default values
                searchIn: {
                    name: true,
                    description: false,
                    readme: false
                }
            }
        });
    }

    public search(): void {
        if (this.mainFormGroup.valid) {
            const filters: SearchFiltersModel = this.mainFormGroup.value;
            this.store.dispatch(new InitiateSearch(filters));
        } else {
            FormUtils.markFormAsDirty(this.mainFormGroup);
            this.showInvalidFiltersMessage();
        }
    }

    private showInvalidFiltersMessage(): void {
        const allErrorNames = FormUtils.getAllErrorNames(this.mainFormGroup);
        const hasOtherThanEmptyFilters = allErrorNames.filter(errorName => errorName !== 'emptyFilters').length > 0;
        const message = hasOtherThanEmptyFilters
            ? {
                detail: 'There are errors in the form. Please correct the fields that are marked with red.',
                summary: 'Search error',
                severity: 'error'
            } : {
                detail: 'Please provide some values to search by.',
                summary: 'Empty search filters',
                severity: 'warn'
            };


        this.store.dispatch(new AddMessage(message));
    }

    public ngOnDestroy(): void {
        this.manualStoreSubscriptions.forEach(sub => sub.unsubscribe());
    }


}
