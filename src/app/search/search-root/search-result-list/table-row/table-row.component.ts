import { Component, Input } from '@angular/core';
import { ResultDisplayModel } from 'src/app/search/models/result/result-display-model';

/**
 * This component is responsible for displaying one given search result.
 * The model it receives is already converted to display format.
 * There is no application logic here, only html and scss.
 */
@Component({
    selector: 'app-table-row',
    templateUrl: './table-row.component.html',
    styleUrls: ['./table-row.component.scss']
})
export class TableRowComponent {

    @Input()
    public rowItem: ResultDisplayModel;

}
