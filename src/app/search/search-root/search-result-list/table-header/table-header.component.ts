import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { SelectItem } from 'primeng/api/selectitem';
import { Observable } from 'rxjs/internal/Observable';
import { SetAsc, SetSortBy, RequestPage } from 'src/app/search/store/search-result-options/search-result-options-actions';
import { SearchResultOptionsState } from 'src/app/search/store/search-result-options/search-result-options-state';

/**
 * This component is the header part for the table that displays the search results.
 * The sorting is implemented here.
 */
@Component({
    selector: 'app-table-header',
    templateUrl: './table-header.component.html',
    styleUrls: ['./table-header.component.scss']
})
export class TableHeaderComponent {

    @Select(SearchResultOptionsState.totalResults)
    public totalResults$: Observable<number>;

    @Select(SearchResultOptionsState.sortBy)
    public sortBy$: Observable<string>;

    @Select(SearchResultOptionsState.isAsc)
    public isAsc$: Observable<boolean>;

    @Select(SearchResultOptionsState.hasSortBy)
    public hasSortBy$: Observable<boolean>;

    public sortByOptions: SelectItem[];
    public orderByOptions: SelectItem[];

    constructor(private readonly store: Store) {
        this.initSortByOptions();
        this.initOrderByOptions();
    }

    private initSortByOptions(): void {
        this.sortByOptions = [
            { label: 'Default', value: null },
            { label: 'Stars', value: 'stars' },
            { label: 'Forks', value: 'forks' }
        ];
    }

    private initOrderByOptions(): void {
        this.orderByOptions = [
            { label: 'Desc', value: false },
            { label: 'Asc', value: true }
        ];
    }

    public setSortBy(sortBy: string): void {
        const prevSortBy = this.store.selectSnapshot(SearchResultOptionsState.sortBy);
        if (sortBy !== prevSortBy) {
            this.store.dispatch(new SetSortBy(sortBy));
            this.store.dispatch(new RequestPage(1));
        }
    }

    public setOrderBy(isAsc: boolean): void {
        const prevIsAsc = this.store.selectSnapshot(SearchResultOptionsState.isAsc);
        if (isAsc !== prevIsAsc) {
            this.store.dispatch(new SetAsc(isAsc));
            this.store.dispatch(new RequestPage(1));
        }
    }


}
