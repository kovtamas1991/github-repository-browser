import { Component, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { SelectItem } from 'primeng/api/selectitem';
import { Observable } from 'rxjs/internal/Observable';
import { Subscription } from 'rxjs/internal/Subscription';
import { RequestPage, RequestNextPage, RequestPreviousPage } from 'src/app/search/store/search-result-options/search-result-options-actions';
import { SearchResultOptionsState } from 'src/app/search/store/search-result-options/search-result-options-state';
import { isNullOrUndefined } from 'util';
import { PaginationModel } from 'src/app/search/models/result/search-result-options-model';

/**
 * This component is the footer part for the table that displays the search results.
 * The pagination is implemented here.
 */
@Component({
    selector: 'app-table-footer',
    templateUrl: './table-footer.component.html',
    styleUrls: ['./table-footer.component.scss']
})
export class TableFooterComponent implements OnDestroy {

    private readonly manualStoreSubscriptions: Subscription[];

    @Select(SearchResultOptionsState.currentPageIndex)
    public currentPageIndex$: Observable<number>;

    @Select(SearchResultOptionsState.hasNextPage)
    public hasNextPage$: Observable<boolean>;

    @Select(SearchResultOptionsState.lastPageIndex)
    public lastPageIndex$: Observable<number>;

    @Select(SearchResultOptionsState.hasPreviousPage)
    public hasPreviousPage$: Observable<boolean>;

    public pageChooserOptions: SelectItem[];

    constructor(private readonly store: Store) {
        this.manualStoreSubscriptions = [];
        this.recalculateOptionsOnPaginationChange();
    }

    private recalculateOptionsOnPaginationChange(): void {
        const subscription = this.store.select(SearchResultOptionsState.pagination).subscribe(pagination => {
            this.pageChooserOptions = [];
            const lastPageIndex = this.calculateLastDiplayablePageIndex(pagination);
            if (!isNullOrUndefined(pagination.lastPageIndex)) {
                for (let i = 1; i <= lastPageIndex; i++) {
                    this.pageChooserOptions.push({ label: `${i}`, value: i });
                }
            }
        });
        this.manualStoreSubscriptions.push(subscription);
    }

    // The index of the last page in which the index of the last element is less then 1000
    private calculateLastDiplayablePageIndex(currentPagination: PaginationModel): number {
        const maximumSupportedResultsByGithub = 1000;
        if (currentPagination.totalResults < maximumSupportedResultsByGithub) {
            return currentPagination.lastPageIndex;
        }

        return Math.floor(maximumSupportedResultsByGithub / currentPagination.itemsPerPage);
    }

    public requestNextPage(): void {
        this.store.dispatch(new RequestNextPage());
    }

    public requestPreviousPage(): void {
        this.store.dispatch(new RequestPreviousPage());
    }

    public requestPage(pageIndex: number): void {
        this.store.dispatch(new RequestPage(pageIndex));
    }

    public ngOnDestroy(): void {
        this.manualStoreSubscriptions.forEach(sub => sub.unsubscribe());
    }

}
