import { Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { ResultDisplayModel } from '../../models/result/result-display-model';
import { SearchResultState } from '../../store/search-result-options/search-result-state';
import { GROW_IN_SHRINK_OUT } from 'src/app/util/animations/animations';

@Component({
    selector: 'app-search-result-list',
    templateUrl: './search-result-list.component.html',
    styleUrls: ['./search-result-list.component.scss'],
    animations: [GROW_IN_SHRINK_OUT]
})
export class SearchResultListComponent {

    @Select(SearchResultState.hasItems)
    public hasItems$: Observable<boolean>;

    @Select(SearchResultState.items)
    public items$: Observable<ResultDisplayModel[]>;

}
