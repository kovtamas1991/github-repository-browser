import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { NotNegative, Required, ValidateArgs } from 'src/app/util/decorators/method-param-validation';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { isNullOrUndefined } from 'util';
import { SearchFiltersModel } from '../models/filter/search-filters-model';
import { SearchResponseModel } from '../models/result/search-response-model';
import { SetUsedQuery } from '../store/search-result-options/search-result-options-actions';
import { SearchResultOptionsState } from '../store/search-result-options/search-result-options-state';
import { QueryBuilderService } from './query-builder.service';

/**
 * This service handles the calls to the github api and gives back the raw response.
 * It doesn't fire any store actions, the store is only injected to select configurations.
 */
@Injectable({
    providedIn: 'root'
})
export class RepositorySearchService {

    constructor(private readonly http: HttpClient,
                private readonly queryBuilder: QueryBuilderService,
                private readonly store: Store) {

    }

    // Start a new search, usually at page 1. It saves the converted query string for later use.
    @ValidateArgs
    public initiateSearch(@Required filters: SearchFiltersModel, @NotNegative requestedPageIndex: number): Observable<SearchResponseModel> {
        const queryString = this.queryBuilder.createSearchParamFromFilters(filters);
        if (queryString === '') {
            throw new Error('Empty search filters were provided to "initiateSearch"');
        }

        return this.search(queryString, requestedPageIndex);
    }

    // Request another page of a search that is already started by a previous initiateSearch
    @ValidateArgs
    public getRequestedPageForUsedQuery(@NotNegative requestedPageIndex: number): Observable<SearchResponseModel> {
        const usedQuery = this.store.selectSnapshot(SearchResultOptionsState.usedQuery);
        if (!validateNotEmpty(usedQuery)) {
            throw new Error('No previously used query string was found in "getRequestedPageForUsedQuery');
        }

        return this.search(usedQuery, requestedPageIndex);
    }

    private search(queryString: string, requestedPageIndex: number): Observable<SearchResponseModel> {
        let params = new HttpParams();
        params = params.append('q', queryString);
        params = this.appendPaginationParams(params, requestedPageIndex);
        params = this.appendSortingParams(params);

        this.store.dispatch(new SetUsedQuery(queryString));
        return this.http.get<SearchResponseModel>('/search/repositories', { params });
    }

    private appendPaginationParams(params: HttpParams, requestedPageIndex: number): HttpParams {
        const itemsPerPage = this.store.selectSnapshot(SearchResultOptionsState.itemsPerPage);
        params = params.append('per_page', itemsPerPage.toString());
        params = params.append('page', requestedPageIndex.toString());
        return params;
    }

    private appendSortingParams(params: HttpParams): HttpParams {
        const sortBy = this.store.selectSnapshot(SearchResultOptionsState.sortBy);
        if (!isNullOrUndefined(sortBy)) {
            const isAsc = this.store.selectSnapshot(SearchResultOptionsState.isAsc);
            const orderBy = isAsc ? 'asc' : 'desc';
            params = params.append('sort', sortBy);
            params = params.append('order', orderBy);
        }

        return params;
    }


}
