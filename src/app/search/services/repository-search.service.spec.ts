import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { SearchFiltersModel } from '../models/filter/search-filters-model';
import { SearchResponseModel } from '../models/result/search-response-model';
import { SearchResultOptionsState } from '../store/search-result-options/search-result-options-state';
import { RepositorySearchService } from './repository-search.service';
import { SearchResultState } from '../store/search-result-options/search-result-state';
import { ResultDisplayModel } from '../models/result/result-display-model';
import { InitiateSearch } from '../store/search-result-options/search-result-options-actions';


const testFilters: SearchFiltersModel = {
    basicSearchFilters: {
        searchText: null,
        searchIn: {
            description: false,
            name: false,
            readme: false
        }
    },
    advancedSearchFilters: {
        languages: ['nodejs', 'assembly', 'typescript', 'angular'],
        username: null,
        organization: null,
        created: {
            operator: {
                displayName: 'Before',
                id: 'BEFORE',
                operatorString: '<'
            },
            values: [new Date('2010-04-29')]
        },
        size: null,
        stars: null,
        topics: null
    }
};

const testResponse: SearchResponseModel = {
    incomplete_results: false,
    total_count: 1,
    items: [
        {
            name: 'Test name',
            full_name: 'Full Test name',
            html_url: 'https:test-url',
            description: 'Test description',
            created_at: '2010-01-12',
            updated_at: '2016-07-14',
            stargazers_count: 298,
            forks_count: 102,
            watchers_count: 102,
            language: 'nodejs',
            open_issues_count: 9,
            owner: {
                avatar_url: 'test-avatar-url',
                login: 'TestUserName',
                html_url: 'https:test-owner-url'
            }
        }
    ]
};

const testConvertedItem: ResultDisplayModel = {
    repositoryName: 'Test name',
    repositoryFullName: 'Full Test name',
    repositoryUrl: 'https:test-url',
    language: 'nodejs',
    created: new Date('2010-01-12'),
    updated: new Date('2016-07-14'),
    description: 'Test description',
    stars: 298,
    watchers: 102,
    forks: 102,
    issues: 9,
    ownerAvatarUrl: 'test-avatar-url',
    ownerLoginName: 'TestUserName',
    ownerWebsiteUrl: 'https:test-owner-url'
};


describe('RepositorySearchService', () => {
    let service: RepositorySearchService;
    let httpMock: HttpTestingController;
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, NgxsModule.forRoot([SearchResultOptionsState, SearchResultState])],
            providers: [RepositorySearchService]
        });
        service = TestBed.inject(RepositorySearchService);
        httpMock = TestBed.inject(HttpTestingController);
        store = TestBed.inject(Store);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('initiateSearch should return mockData', () => {
        service.initiateSearch(testFilters, 1).subscribe(response => {
            expect(response).toEqual(testResponse);
        });
        const req = httpMock.expectOne('/search/repositories?q=language:nodejs+language:assembly+language:typescript+language:angular+created:%3C2010-04-29&per_page=10&page=1');
        expect(req.request.method).toBe('GET');
        req.flush(testResponse);
    });

    it('store should contain converted result items', () => {
        // service.initiateSearch(testFilters, 1).toPromise().then();
        store.dispatch(new InitiateSearch(testFilters));
        const req = httpMock.expectOne('/search/repositories?q=language:nodejs+language:assembly+language:typescript+language:angular+created:%3C2010-04-29&per_page=10&page=1');
        req.flush(testResponse);

        const convertedItems = store.selectSnapshot(SearchResultState.items);
        expect(convertedItems.length).toBe(1);
        expect(convertedItems[0]).toEqual(testConvertedItem);
    });

    afterEach(() => {
        httpMock.verify();
    });

});
