import { Injectable } from '@angular/core';
import { format } from 'date-fns';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { isNullOrUndefined, isNumber } from 'util';
import { OperatorModel } from '../models/values-with-operator/operator-model';
import { AdvancedSearchFiltersModel, BasicSearchFiltersModel, SearchFiltersModel } from '../models/filter/search-filters-model';

/**
 * This service is responsible for converting the search filters into a string
 * that the github api can understand. This string will go into the right side of the
 * q=... part of the url.
 */
@Injectable({
    providedIn: 'root'
})
export class QueryBuilderService {

    public createSearchParamFromFilters(filters: SearchFiltersModel): string {
        const basicParams = this.createBasicSearchParams(filters.basicSearchFilters);
        const advancedParams = this.createAdvancedSearchParams(filters.advancedSearchFilters);
        return this.joinFiltersParams(basicParams, advancedParams);
    }

    // Convert the part that only includes the searchText and the categories to search in
    private createBasicSearchParams(filters: BasicSearchFiltersModel): string {
        if (!validateNotEmpty(filters.searchText)) {
            return '';
        }

        const searchInParts = [];
        Object.keys(filters.searchIn).forEach(searchInName => {
            if (filters.searchIn[searchInName]) {
                searchInParts.push(`in:${searchInName}`);
            }
        });

        if (searchInParts.length === 0) {
            return '';
        }

        return `${filters.searchText}+${searchInParts.join('+')}`;
    }

    // When there are multiple search params, this method puts the + sign between them
    private joinFiltersParams(...filterParams: string[]): string {
        return filterParams.filter(param => param !== '').join('+');
    }

    // Convert the advanced search params if present.
    private createAdvancedSearchParams(filters: AdvancedSearchFiltersModel): string {
        if (isNullOrUndefined(filters)) {
            return '';
        }

        const usernameParam = this.fromSimpleStringValue('username', filters.username);
        const organizationParam = this.fromSimpleStringValue('organization', filters.organization);

        const languagesParam = this.fromArrayValue('language', filters.languages);
        const topicsParam = this.fromArrayValue('topic', filters.topics);

        const starsParam = this.fromValuesWithOperator('stars', filters.stars?.operator, filters.stars?.values);
        const createdParam = this.fromValuesWithOperator('created', filters.created?.operator, filters.created?.values);
        const sizeParam = this.fromValuesWithOperator('size', filters.size?.operator, filters.size?.values);

        return this.joinFiltersParams(
            usernameParam,
            organizationParam,
            languagesParam,
            topicsParam,
            starsParam,
            createdParam,
            sizeParam
        );
    }

    private fromSimpleStringValue(name: string, value: string): string {
        if (!validateNotEmpty(value)) {
            return '';
        }
        return `${name}:${encodeURIComponent(value)}`;
    }

    private fromArrayValue(name: string, values: string[]): string {
        if (!validateNotEmpty(values)) {
            return '';
        }
        return values
            .map(currentValue => this.fromSimpleStringValue(name, currentValue))
            .join('+');
    }

    private fromValuesWithOperator(name: string, operator: OperatorModel, values: (Date | number)[]): string {
        if (!validateNotEmpty(values)) {
            return '';
        }

        return isNumber(values[0])
            ? this.fromNumberValuesWithOperator(name, operator, values as number[])
            : this.fromDateValuesWithOperator(name, operator, values as Date[]);
    }


    private fromNumberValuesWithOperator(name: string, operator: OperatorModel, values: number[]): string {
        const stringValues = values.map(currentValue => currentValue.toString());
        return this.fromStringValueswithOperator(name, operator, stringValues);
    }

    private fromDateValuesWithOperator(name: string, operator: OperatorModel, values: Date[]): string {
        const dateStrings = values.map(date => format(date, 'yyyy-MM-dd'));
        return this.fromStringValueswithOperator(name, operator, dateStrings);
    }

    private fromStringValueswithOperator(name: string, operator: OperatorModel, values: string[]): string {
        const encodedValues = values.map(encodeURIComponent);
        if (operator.id === 'BETWEEN') {
            return `${name}:${encodedValues.join(operator.operatorString)}`;
        }

        return `${name}:${operator.operatorString}${encodedValues[0]}`;
    }

}
