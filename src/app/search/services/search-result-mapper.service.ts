import { Injectable } from '@angular/core';
import { ResultDisplayModel } from '../models/result/result-display-model';
import { SearchResponseItemModel } from '../models/result/search-response-model';

/**
 * The raw response given by the github api uses a different style in variable naming
 * and they can change it in the future. Using this mapper, this class and the corresponding
 * SearchResponseItemModel interface are the only parts of the program that are directly
 * dependent on the structure of the api response.
 */
@Injectable({
    providedIn: 'root'
})
export class SearchResultMapperService {

    public toDisplayFormat(searchResponse: SearchResponseItemModel): ResultDisplayModel {
        return {
            repositoryName: searchResponse.name,
            repositoryFullName: searchResponse.full_name,
            repositoryUrl: searchResponse.html_url,
            stars: searchResponse.stargazers_count,
            watchers: searchResponse.watchers_count,
            forks: searchResponse.forks_count,
            issues: searchResponse.open_issues_count,
            description: searchResponse.description,
            language: searchResponse.language,
            created: new Date(searchResponse.created_at),
            updated: new Date(searchResponse.updated_at),
            ownerLoginName: searchResponse.owner?.login,
            ownerAvatarUrl: searchResponse.owner?.avatar_url,
            ownerWebsiteUrl: searchResponse.owner?.html_url
        };
    }

}
