import { TestBed } from '@angular/core/testing';

import { QueryBuilderService } from './query-builder.service';
import { SearchFiltersModel, AdvancedSearchFiltersModel } from '../models/filter/search-filters-model';
import { NgxsModule, Store } from '@ngxs/store';
import { OperatorsState } from '../store/operators-state';

function createDefaultFilters(): SearchFiltersModel {
    return {
        basicSearchFilters: {
            searchText: null,
            searchIn: {
                name: true,
                description: false,
                readme: false
            }
        },
        advancedSearchFilters: null
    };
}

function createEmptyAdvancedSearchFilters(): AdvancedSearchFiltersModel {
    return {
        username: null,
        organization: null,
        languages: null,
        topics: null,
        stars: null,
        created: null,
        size: null
    };
}

describe('QueryBuilderService', () => {
    let service: QueryBuilderService;
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([OperatorsState])]
        });

        service = TestBed.inject(QueryBuilderService);
        store = TestBed.inject(Store);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return empty string when using the default filters', () => {
        const filters: SearchFiltersModel = createDefaultFilters();
        const queryString = service.createSearchParamFromFilters(filters);
        expect(queryString).toBe('');
    });

    it('should only contain the advanced search part', () => {
        const filters = createDefaultFilters();
        filters.advancedSearchFilters = createEmptyAdvancedSearchFilters();
        const testUserName = 'TestUserName';
        filters.advancedSearchFilters.username = testUserName;
        const queryString = service.createSearchParamFromFilters(filters);
        expect(queryString).toBe(`username:${testUserName}`);
    });

    it ('should include every filter when all filled', () => {
        const filters = createDefaultFilters();
        filters.advancedSearchFilters = createEmptyAdvancedSearchFilters();
        filters.basicSearchFilters.searchText = 'SearchText';
        filters.basicSearchFilters.searchIn.description = true;
        filters.basicSearchFilters.searchIn.readme = true;

        filters.advancedSearchFilters.username = 'UserName';
        filters.advancedSearchFilters.organization = 'OrganizationName';
        filters.advancedSearchFilters.languages = ['nodejs', 'typescript', 'angular'];
        filters.advancedSearchFilters.topics = ['fullstack', 'rapid', 'development'];

        const numberOperators = store.selectSnapshot(OperatorsState.numberOperators);
        const dateOperators = store.selectSnapshot(OperatorsState.dateOperators);

        filters.advancedSearchFilters.stars = {
            operator: numberOperators.find(op => op.id === 'GREATER_THAN'),
            values: [1]
        };
        filters.advancedSearchFilters.created = {
            operator: dateOperators.find(op => op.id === 'BEFORE'),
            values: [new Date('2020-01-01')]
        };
        filters.advancedSearchFilters.size = {
            operator: numberOperators.find(op => op.id === 'BETWEEN'),
            values: [100, 900]
        };

        const queryString = service.createSearchParamFromFilters(filters);
        expect(queryString.startsWith(`${filters.basicSearchFilters.searchText}`)).toBeTrue();
        expect(queryString).toContain(`in:name`);
        expect(queryString).toContain(`in:description`);
        expect(queryString).toContain(`in:readme`);

        expect(queryString).toContain(`username:${filters.advancedSearchFilters.username}`);
        expect(queryString).toContain(`organization:${filters.advancedSearchFilters.organization}`);

        expect(queryString).toContain(`language:${filters.advancedSearchFilters.languages[0]}`);
        expect(queryString).toContain(`language:${filters.advancedSearchFilters.languages[1]}`);
        expect(queryString).toContain(`language:${filters.advancedSearchFilters.languages[2]}`);

        expect(queryString).toContain(`topic:${filters.advancedSearchFilters.topics[0]}`);
        expect(queryString).toContain(`topic:${filters.advancedSearchFilters.topics[1]}`);
        expect(queryString).toContain(`topic:${filters.advancedSearchFilters.topics[2]}`);

        expect(queryString).toContain(`stars:>1`);
        expect(queryString).toContain(`created:<2020-01-01`);
        expect(queryString).toContain(`size:100..900`);



    });

});
