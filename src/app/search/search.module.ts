import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InputTextModule } from 'primeng/inputtext';
import { SliderModule } from 'primeng/slider';
import { SearchFiltersComponent } from './search-root/search-filters/search-filters.component';
import { SearchResultListComponent } from './search-root/search-result-list/search-result-list.component';
import { SearchRootComponent } from './search-root/search-root.component';
import { SearchRoutingModule } from './search-routing.module';
import { SearchConfigurationState } from './store/search-configuration-state';
import { ChipsModule } from 'primeng/chips';
import { ValuesWithOperatorComponent } from './search-root/search-filters/values-with-operator/values-with-operator.component';
import { OperatorsState } from './store/operators-state';
import { SearchResultOptionsState } from './store/search-result-options/search-result-options-state';
import { SearchResultState } from './store/search-result-options/search-result-state';
import { TableHeaderComponent } from './search-root/search-result-list/table-header/table-header.component';
import { TableRowComponent } from './search-root/search-result-list/table-row/table-row.component';
import { TableFooterComponent } from './search-root/search-result-list/table-footer/table-footer.component';
import { DateValuesComponent } from './search-root/search-filters/values-with-operator/date-values/date-values.component';
import { NumberValuesComponent } from './search-root/search-filters/values-with-operator/number-values/number-values.component';
import { ErrorMessageDisplayerComponent } from './validators/error-message-displayer/error-message-displayer.component';


@NgModule({
    declarations: [SearchRootComponent, SearchFiltersComponent, SearchResultListComponent, ValuesWithOperatorComponent, TableHeaderComponent, TableRowComponent, TableFooterComponent, DateValuesComponent, NumberValuesComponent, ErrorMessageDisplayerComponent],
    imports: [
        CommonModule,
        SearchRoutingModule,
        NgxsModule.forFeature([SearchConfigurationState, OperatorsState, SearchResultOptionsState, SearchResultState]),
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        CheckboxModule,
        SliderModule,
        RadioButtonModule,
        SelectButtonModule,
        DropdownModule,
        ButtonModule,
        ChipsModule,
        CalendarModule,
        MessagesModule,
        MessageModule
    ]
})
export class SearchModule { }
