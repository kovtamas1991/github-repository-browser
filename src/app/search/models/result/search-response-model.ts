export interface SearchResponseModel {

    incomplete_results: boolean;
    items: SearchResponseItemModel[];
    total_count: number;

}

export interface SearchResponseItemModel {

    name: string;
    full_name: string;
    html_url: string;

    stargazers_count: number;
    watchers_count: number;
    forks_count: number;

    open_issues_count: number;

    description: string;
    language: string;

    created_at: string;
    updated_at: string;

    owner: OwnerModel;

    // The above properties are not all that the server sends,
    // they are only the ones that this application needs.
    [key: string]: any;

}

export interface OwnerModel {

    login: string;
    avatar_url: string;
    html_url: string;

    [key: string]: any;

}
