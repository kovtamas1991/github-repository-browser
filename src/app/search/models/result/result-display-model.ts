export interface ResultDisplayModel {

    repositoryName: string;
    repositoryFullName: string;
    repositoryUrl: string;

    stars: number;
    watchers: number;
    forks: number;
    issues: number;

    description: string;

    language: string;

    created: Date;
    updated: Date;

    ownerLoginName: string;
    ownerAvatarUrl: string;
    ownerWebsiteUrl?: string;

}
