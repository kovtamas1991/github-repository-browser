export interface SearchResultOptionsModel {

    usedQuery: string;
    sorting: SortingModel;
    pagination: PaginationModel;

}


export interface SortingModel {

    sortBy: string;
    asc: boolean;

}

export interface PaginationModel {

    itemsPerPage: number;
    totalResults: number;
    lastPageIndex: number;
    currentPageIndex: number;

    hasNextPage: boolean;
    hasPreviousPage: boolean;

}
