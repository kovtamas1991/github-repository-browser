export interface OperatorModel {

    displayName: string;
    id: string;
    operatorString: string;

}
