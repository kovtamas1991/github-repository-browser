import { OperatorModel } from './operator-model';

export interface ValuesWithOperatorModel<T> {

    operator: OperatorModel;
    values: T[];

}
