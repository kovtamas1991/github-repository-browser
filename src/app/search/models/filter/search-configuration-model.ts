export interface SearchConfigurationModel {

    searchMode: SearchModeModel;

}

export interface SearchModeModel {

    isAdvanced: boolean;
    toggleIcon: string;
    toAdvancedIcon: string;
    toSimpleIcon: string;

}
