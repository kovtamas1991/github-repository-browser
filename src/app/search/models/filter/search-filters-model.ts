import { ValuesWithOperatorModel } from '../values-with-operator/values-with-operator-model';

export interface SearchFiltersModel {

    basicSearchFilters: BasicSearchFiltersModel;
    advancedSearchFilters?: AdvancedSearchFiltersModel;

}

export interface BasicSearchFiltersModel {

    searchText: string;
    searchIn: SearchInModel;

}

export interface SearchInModel {

    name: boolean;
    description: boolean;
    readme: boolean;

}

export interface AdvancedSearchFiltersModel {

    username: string;
    organization: string;

    languages: string[];
    topics: string[];

    stars: ValuesWithOperatorModel<number>;
    created: ValuesWithOperatorModel<Date>;
    size: ValuesWithOperatorModel<number>;

}
