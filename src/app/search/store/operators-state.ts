import { Injectable } from '@angular/core';
import { Selector, State } from '@ngxs/store';
import { AllArgsNotEmpty } from 'src/app/util/decorators/method-param-validation';
import { OperatorModel } from '../models/values-with-operator/operator-model';

/*
    The operators declared here are used in AdvancedSearch mode, with
    the ValuesWithOperatorComponent. The value of the selected operator
    will take part in determining how the search query gets created.
    This "OperatorsState" is like a readonly database. There are no
    actions to change them. But I personally prefer to put them in the store
    rather than using "export const" or putting them into a static field
    of some component.
*/

export interface OperatorStateModel {

    operatorsForTypes: Map<string, OperatorModel[]>;

}

function createDefaultState(): OperatorStateModel {
    let operatorsForNumberType: OperatorModel[] = [
        { displayName: 'Equal', id: 'EQUAL', operatorString: '' },
        { displayName: 'Greater than', id: 'GREATER_THAN', operatorString: '>' },
        { displayName: 'Less than', id: 'LESS_THAN', operatorString: '<' },
        { displayName: 'Between', id: 'BETWEEN', operatorString: '..' }
    ];
    operatorsForNumberType = operatorsForNumberType.map(op => Object.freeze(op));
    Object.freeze(operatorsForNumberType);

    let operatorsForDateType: OperatorModel[] = [
        { displayName: 'Before', id: 'BEFORE', operatorString: '<' },
        { displayName: 'On or before', id: 'ON_OR_BEFORE', operatorString: '<=' },
        { displayName: 'After', id: 'AFTER', operatorString: '>' },
        { displayName: 'On or after', id: 'ON_OR_AFTER', operatorString: '>=' },
        { displayName: 'Between', id: 'BETWEEN', operatorString: '..' }
    ];
    operatorsForDateType = operatorsForDateType.map(op => Object.freeze(op));
    Object.freeze(operatorsForDateType);

    const operatorsMap = new Map<string, OperatorModel[]>();
    operatorsMap.set('number', operatorsForNumberType);
    operatorsMap.set('date', operatorsForDateType);

    return { operatorsForTypes: operatorsMap };
}

@State<OperatorStateModel>({
    name: 'operatorsForTypes',
    defaults: createDefaultState()
})
@Injectable()
export class OperatorsState {

    @AllArgsNotEmpty
    public static getSelectorForType(type: string): (state: OperatorStateModel) => OperatorModel[] {
        switch (type) {
            case 'number':
                return OperatorsState.numberOperators;
            case 'date':
                return OperatorsState.dateOperators;
            default:
                throw new Error(`Unknown operator type: ${type}`);
        }
    }

    @Selector()
    public static numberOperators(state: OperatorStateModel): OperatorModel[] {
        return state.operatorsForTypes.get('number');
    }

    @Selector()
    public static dateOperators(state: OperatorStateModel): OperatorModel[] {
        return state.operatorsForTypes.get('date');
    }

}
