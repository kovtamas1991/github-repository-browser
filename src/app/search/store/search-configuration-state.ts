import { State, Selector, Action, StateContext } from '@ngxs/store';
import { SearchConfigurationModel } from '../models/filter/search-configuration-model';

export class SetIsAdvanced {
    public static readonly type = '[SEARCH_CONFIGURATION] setIsAdvanced';
    constructor(public readonly payload: boolean) { }
}

export class ToggleAdvancedMode {
    public static readonly type = '[SEARCH_CONFIGURATION] toggleAdvancedMode';
}

const defaultState: SearchConfigurationModel = {
    searchMode: {
        isAdvanced: false,
        toggleIcon: 'pi pi-chevron-down',
        toAdvancedIcon: 'pi pi-chevron-down',
        toSimpleIcon: 'pi pi-chevron-up'
    }
};

@State<SearchConfigurationModel>({
    name: 'searchConfiguration',
    defaults: defaultState
})
export class SearchConfigurationState {

    @Selector()
    public static isAdvanced(state: SearchConfigurationModel): boolean {
        return state.searchMode.isAdvanced;
    }

    @Selector()
    public static toggleIcon(state: SearchConfigurationModel): string {
        return state.searchMode.toggleIcon;
    }

    @Action(ToggleAdvancedMode)
    public toggleAdvancedMode(ctx: StateContext<SearchConfigurationModel>): void {
        const state = ctx.getState();
        const isAdvancedNow = state.searchMode.isAdvanced;
        const willBeAdvanced = !isAdvancedNow;
        const newToggleIcon = willBeAdvanced
            ? state.searchMode.toSimpleIcon
            : state.searchMode.toAdvancedIcon;

        ctx.patchState({
            searchMode: {
                ...state.searchMode,
                isAdvanced: willBeAdvanced,
                toggleIcon: newToggleIcon
            }
        });
    }

}

