import { SortingModel, PaginationModel } from '../../models/result/search-result-options-model';
import { SearchFiltersModel } from '../../models/filter/search-filters-model';

/**
 * Declarations of the store actions used in the SearchResultOptionsState class.
 */

const typeName = '[SEARCH_RESULT_OPTIONS]';

export class SetUsedQuery {
    public static readonly type = `${typeName} setUsedQuery`;
    constructor(public readonly payload: string) {}
}

// Sorting setter actions
export class SetSorting {
    public static readonly type = `${typeName} setSorting`;
    constructor(public readonly payload: SortingModel) {}
}

export class SetSortBy {
    public static readonly type = `${typeName} setSortBy`;
    constructor(public readonly payload: string) {}
}

export class SetAsc {
    public static readonly type = `${typeName} setAsc`;
    constructor(public readonly payload: boolean) {}
}

// Pagination setter actions
export class SetPagination {
    public static readonly type = `${typeName} setPagination`;
    constructor(public readonly payload: PaginationModel) {}
}

export class SetItemsPerPage {
    public static readonly type = `${typeName} setItemsPerPage`;
    constructor(public readonly payload: number) {}
}

export class SetTotalResults {
    public static readonly type = `${typeName} setTotalResults`;
    constructor(public readonly payload: number) {}
}

export class SetLastPageIndex {
    public static readonly type = `${typeName} SetLastPageIndex`;
    constructor(public readonly payload: number) {}
}

export class SetCurrentPageIndex {
    public static readonly type = `${typeName} SetCurrentPageIndex`;
    constructor(public readonly payload: number) {}
}

export class SetHasPreviousPage {
    public static readonly type = `${typeName} setHasPreviousPage`;
    constructor(public readonly payload: boolean) {}
}

export class SetHasNextPage {
    public static readonly type = `${typeName} setHasNextPage`;
    constructor(public readonly payload: boolean) {}
}

// Request actions
export class InitiateSearch {
    public static readonly type = `${typeName} initiateSearch`;
    constructor(public readonly payload: SearchFiltersModel) {}
}

export class RequestNextPage {
    public static readonly type = `${typeName} requestNextPage`;
}

export class RequestPreviousPage {
    public static readonly type = `${typeName} requestPreviousPage`;
}

export class RequestPage {
    public static readonly type = `${typeName} requestPage`;
    constructor(public readonly payload: number) {}
}

