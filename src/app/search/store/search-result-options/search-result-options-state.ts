import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SearchResponseModel } from '../../models/result/search-response-model';
import { PaginationModel, SearchResultOptionsModel, SortingModel } from '../../models/result/search-result-options-model';
import { RepositorySearchService } from '../../services/repository-search.service';
import { SearchResultMapperService } from '../../services/search-result-mapper.service';
import { InitiateSearch, RequestNextPage, RequestPage, RequestPreviousPage, SetAsc, SetCurrentPageIndex, SetHasNextPage, SetHasPreviousPage, SetItemsPerPage, SetLastPageIndex, SetPagination, SetSortBy, SetSorting, SetTotalResults, SetUsedQuery } from './search-result-options-actions';
import { SetItems } from './search-result-state';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/internal/operators/tap';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { AddMessage, MessageModel } from 'src/app/store/messages-state';

const defaultState: SearchResultOptionsModel = {
    usedQuery: null,
    sorting: {
        sortBy: null,
        asc: null
    },
    pagination: {
        itemsPerPage: 10,
        totalResults: null,
        lastPageIndex: null,
        currentPageIndex: null,
        hasNextPage: false,
        hasPreviousPage: false
    }
};

/**
 * This state holds the configurations for sorting, pagination.
 * Also this state provides the actions to start a new search or request another page
 * of this current search results.
 */
@State<SearchResultOptionsModel>({
    name: 'searchResultOptions',
    defaults: defaultState
})
@Injectable()
export class SearchResultOptionsState {

    constructor(private readonly searchService: RepositorySearchService,
        private readonly mapper: SearchResultMapperService) { }

    @Selector()
    public static usedQuery(state: SearchResultOptionsModel): string {
        return state.usedQuery;
    }

    @Selector()
    public static sorting(state: SearchResultOptionsModel): SortingModel {
        return { ...state.sorting };
    }

    @Selector()
    public static sortBy(state: SearchResultOptionsModel): string {
        return state.sorting.sortBy;
    }

    @Selector()
    public static hasSortBy(state: SearchResultOptionsModel): boolean {
        return validateNotEmpty(state.sorting.sortBy);
    }

    @Selector()
    public static isAsc(state: SearchResultOptionsModel): boolean {
        return state.sorting.asc;
    }

    @Selector()
    public static pagination(state: SearchResultOptionsModel): PaginationModel {
        return { ...state.pagination };
    }

    @Selector()
    public static itemsPerPage(state: SearchResultOptionsModel): number {
        return state.pagination.itemsPerPage;
    }
    @Selector()
    public static totalResults(state: SearchResultOptionsModel): number {
        return state.pagination.totalResults;
    }
    @Selector()
    public static lastPageIndex(state: SearchResultOptionsModel): number {
        return state.pagination.lastPageIndex;
    }
    @Selector()
    public static currentPageIndex(state: SearchResultOptionsModel): number {
        return state.pagination.currentPageIndex;
    }
    @Selector()
    public static hasNextPage(state: SearchResultOptionsModel): boolean {
        return state.pagination.hasNextPage;
    }
    @Selector()
    public static hasPreviousPage(state: SearchResultOptionsModel): boolean {
        return state.pagination.hasPreviousPage;
    }

    @Action(SetUsedQuery)
    public setUsedQuery(ctx: StateContext<SearchResultOptionsModel>, action: SetUsedQuery): void {
        ctx.patchState({ usedQuery: action.payload });
    }

    // Sorting setter actions
    @Action(SetSorting)
    public setSorting(ctx: StateContext<SearchResultOptionsModel>, action: SetSorting): void {
        ctx.patchState({ sorting: action.payload });
    }

    @Action(SetSortBy)
    public setSortBy(ctx: StateContext<SearchResultOptionsModel>, action: SetSortBy): void {
        ctx.patchState({
            sorting: { ...ctx.getState().sorting, sortBy: action.payload }
        });
    }

    @Action(SetAsc)
    public setAsc(ctx: StateContext<SearchResultOptionsModel>, action: SetAsc): void {
        ctx.patchState({
            sorting: { ...ctx.getState().sorting, asc: action.payload }
        });
    }

    // Pagination setter actions
    @Action(SetPagination)
    public setPagination(ctx: StateContext<SearchResultOptionsModel>, action: SetPagination): void {
        ctx.patchState({ pagination: action.payload });
    }

    @Action(SetItemsPerPage)
    public setItemsPerPage(ctx: StateContext<SearchResultOptionsModel>, action: SetItemsPerPage): void {
        ctx.patchState({
            pagination: { ...ctx.getState().pagination, itemsPerPage: action.payload }
        });
    }

    @Action(SetTotalResults)
    public setTotalResults(ctx: StateContext<SearchResultOptionsModel>, action: SetTotalResults): void {
        ctx.patchState({
            pagination: { ...ctx.getState().pagination, totalResults: action.payload }
        });
    }

    @Action(SetLastPageIndex)
    public setLastPageIndex(ctx: StateContext<SearchResultOptionsModel>, action: SetLastPageIndex): void {
        ctx.patchState({
            pagination: { ...ctx.getState().pagination, lastPageIndex: action.payload }
        });
    }

    @Action(SetCurrentPageIndex)
    public setCurrentPageIndex(ctx: StateContext<SearchResultOptionsModel>, action: SetCurrentPageIndex): void {
        ctx.patchState({
            pagination: { ...ctx.getState().pagination, currentPageIndex: action.payload }
        });
    }

    @Action(SetHasNextPage)
    public setHasNextPage(ctx: StateContext<SearchResultOptionsModel>, action: SetHasNextPage): void {
        ctx.patchState({
            pagination: { ...ctx.getState().pagination, hasNextPage: action.payload }
        });
    }

    @Action(SetHasPreviousPage)
    public setHasPreviousPage(ctx: StateContext<SearchResultOptionsModel>, action: SetHasPreviousPage): void {
        ctx.patchState({
            pagination: { ...ctx.getState().pagination, hasPreviousPage: action.payload }
        });
    }

    // Starts a new search. Displayes a warning message when there are too many results
    @Action(InitiateSearch)
    public initiateSearch(ctx: StateContext<SearchResultOptionsModel>, action: InitiateSearch): Observable<SearchResponseModel> {
        const pageIndex = 1;
        return this.searchService.initiateSearch(action.payload, pageIndex)
            .pipe(tap(response => {
                this.handleSearchResult(ctx, response, pageIndex);
                this.printWarningIfTooManyResults(ctx);
            }));
    }

    @Action(RequestNextPage)
    public requestNextPage(ctx: StateContext<SearchResultOptionsModel>): Observable<void> {
        const pageIndex = ctx.getState().pagination.currentPageIndex + 1;
        return ctx.dispatch(new RequestPage(pageIndex));
    }

    @Action(RequestPreviousPage)
    public requestPreviousPage(ctx: StateContext<SearchResultOptionsModel>): Observable<void> {
        const pageIndex = ctx.getState().pagination.currentPageIndex - 1;
        return ctx.dispatch(new RequestPage(pageIndex));
    }

    // Request a certain page of the current search results
    @Action(RequestPage)
    public requestPage(ctx: StateContext<SearchResultOptionsModel>, action: RequestPage): Observable<SearchResponseModel> {
        const pagination = ctx.getState().pagination;
        const requestedPageIndex = action.payload;
        if (requestedPageIndex < 1) {
            throw new Error('Cannot request a page with an index that is less than 1!');
        }
        if (requestedPageIndex > pagination.lastPageIndex) {
            throw new Error(`The last page index is ${pagination.lastPageIndex}!
                Cannot fetch the ${requestedPageIndex}th page.`);
        }

        return this.searchService.getRequestedPageForUsedQuery(requestedPageIndex)
            .pipe(tap(response => this.handleSearchResult(ctx, response, requestedPageIndex)));
    }

    // Creates the new pagination configurations and sets the items to be displayed
    private handleSearchResult(ctx: StateContext<SearchResultOptionsModel>, response: SearchResponseModel, pageIndex: number): void {
        const state = ctx.getState();
        const totalResults = response.total_count;
        const lastPageIndex = Math.ceil(totalResults / state.pagination.itemsPerPage);
        const hasNextPage = lastPageIndex > pageIndex;
        const hasPreviousPage = pageIndex > 1;
        const newPagination: PaginationModel = {
            ...state.pagination,
            currentPageIndex: pageIndex,
            hasPreviousPage,
            hasNextPage,
            totalResults,
            lastPageIndex
        };
        const displayItems = response.items.map(this.mapper.toDisplayFormat);

        ctx.dispatch(new SetPagination(newPagination));
        ctx.dispatch(new SetItems(displayItems));
    }

    // Only called when initiating a new search. Otherwise every page request would possibly result in a message displayed
    private printWarningIfTooManyResults(ctx: StateContext<SearchResultOptionsModel>): void {
        const totalResults = ctx.getState().pagination.totalResults;
        const maximumSupportedResultsByGithub = 1000;
        if (totalResults > maximumSupportedResultsByGithub) {
            const message: MessageModel = {
                summary: 'Too many results!',
                detail: `Github only supports the first ${maximumSupportedResultsByGithub} number of results! The results after that won't be displayed.`,
                severity: 'warn',
                life: 5000
            };
            ctx.dispatch(new AddMessage(message));
        }
    }

}
