import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { ResultDisplayModel } from '../../models/result/result-display-model';

export interface SearchResultModel {

    items: ResultDisplayModel[];

}

const typeName = '[SEARCH_RESULT]';

export class SetItems {
    public static readonly type = `${typeName} Set items`;
    constructor(public readonly payload: ResultDisplayModel[]) {}
}

export class ResetResults {
    public static readonly type = `${typeName} Reset`;
}

@State<SearchResultModel>({
    name: 'searchResults',
    defaults: {
        items: []
    }
})
@Injectable()
export class SearchResultState {

    @Selector()
    public static items(state: SearchResultModel): ResultDisplayModel[] {
        return state.items;
    }

    @Selector()
    public static hasItems(state: SearchResultModel): boolean {
        return validateNotEmpty(state.items);
    }

    @Action(SetItems)
    public setItems(ctx: StateContext<SearchResultModel>, action: SetItems): void {
        ctx.setState({
            items: action.payload
        });
    }

    @Action(ResetResults)
    public resetResults(ctx: StateContext<SearchResultModel>, action: ResetResults): void {
        ctx.setState({
            items: []
        });
    }

}
