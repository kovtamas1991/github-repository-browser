import { ValidationErrors } from '@angular/forms';
import { isNullOrUndefined } from 'util';

export const customValidationErrors = {
    minmax: {
        minmax: true
    },
    fromToDateError: {
        fromToDateError: true
    },
    noSearchInCategory: {
        noSearchInCategory: true
    },
    emptyFilters: {
        emptyFilters: true
    }
};

interface ErrorMessageObject {
    name: string;
    fn: (error?: any) => string;
}

const errorMessageFunctionsInPriorityOrder: ErrorMessageObject[] = [
    { name: 'required', fn: () => 'Field is required!' },
    { name: 'minlength', fn: (error: { actualLength: number, requiredLength: number }) => `Minimum length: ${error.requiredLength}` },
    { name: 'noSearchInCategory', fn: () => 'Select at least one!' },
    { name: 'minmax', fn: () => 'The min value must be smaller than the max!' },
    { name: 'fromToDateError', fn: () => 'From date must be a sooner time than to date!' }
];

export function getHighestPriorityErrorMessage(errors: ValidationErrors): string {
    if (isNullOrUndefined(errors)) {
        return null;
    }


    const errorObj = errorMessageFunctionsInPriorityOrder
        .find(currentError => !isNullOrUndefined(errors[currentError.name]));

    if (isNullOrUndefined(errorObj)) {
        return 'Unknown field error!';
    }

    return errorObj.fn(errors[errorObj.name]);
}
