import { FormGroup, ValidationErrors, FormArray } from '@angular/forms';
import { FormUtils } from 'src/app/util/forms/form-utils';
import { hasSelectedCategory, hasSearchText, hasAdvancedSearchFilter, anyNullOrUndefined } from './validator-utils';

export function hasSearchInForSearchText(basicFiltersGroup: FormGroup): ValidationErrors | null {
    const searchInGroup = basicFiltersGroup.controls['searchIn'] as FormGroup;
    const hasSelected = hasSelectedCategory(searchInGroup);
    const hasText = hasSearchText(basicFiltersGroup);
    const areControlsValid = hasSelected || !hasText;

    const searchInControlNames = Object.keys(searchInGroup.controls);
    const errors = areControlsValid ? null : { noSearchInCategory: true };
    searchInControlNames.forEach(currentName => {
        const currentControl = searchInGroup.controls[currentName];
        currentControl.markAsDirty();
        currentControl.setErrors(FormUtils.copyErrors(errors));
    });

    return FormUtils.copyErrors(errors);
}

export function requireFilterValue(mainFormGroup: FormGroup): ValidationErrors | null {
    const basicFiltersGroup = mainFormGroup.controls['basicSearchFilters'] as FormGroup;
    const hasText = hasSearchText(basicFiltersGroup);
    const hasAdvancedFilter = hasAdvancedSearchFilter(mainFormGroup.controls['advancedSearchFilters'] as FormGroup);
    const hasFilterValue = hasText || hasAdvancedFilter;
    return hasFilterValue ? null : { emptyFilters: true };
}

// Makes the form invalid if the from date is a later time than the to date.
export function validateFromTo(formArray: FormArray): ValidationErrors | null {
    const controls = formArray.controls;
    if (controls.length === 1) {
        return null;
    }

    const fromDate = controls[0].value;
    const toDate = controls[1].value;

    if (anyNullOrUndefined(fromDate, toDate)) {
        return null;
    }

    const isFromToValid = fromDate < toDate;
    const errors = isFromToValid ? null : { fromToDateError: true };
    controls.forEach(control => control.setErrors(FormUtils.copyErrors(errors)));

    return errors;
}

// Makes the form invalid and dirty when the min value is greater than the max value
export function minMaxValidator(group: FormGroup): ValidationErrors | null {
    const minControl = group.controls['min'];
    const maxControl = group.controls['max'];
    const min = minControl.value;
    const max = maxControl.value;
    if (anyNullOrUndefined(min, max)) {
        return null;
    }

    const isMinmaxValid = min < max;
    const errors = isMinmaxValid ? null : { minmax: true };

    minControl.setErrors(FormUtils.copyErrors(errors));
    maxControl.setErrors(FormUtils.copyErrors(errors));

    if (errors !== null) {
        FormUtils.markFormAsDirty(group);
    }

    return FormUtils.copyErrors(errors);
}

