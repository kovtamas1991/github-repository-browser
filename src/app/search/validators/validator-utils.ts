import { FormGroup } from '@angular/forms';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { isNullOrUndefined } from 'util';

export function hasSelectedCategory(searchInGroup: FormGroup): boolean {
    const controlNames = Object.keys(searchInGroup.controls);
    return controlNames.some(currentName => searchInGroup.controls[currentName].value);
}

export function hasSearchText(basicFiltersGroup: FormGroup): boolean {
    const searchText = basicFiltersGroup.controls['searchText'].value;
    return validateNotEmpty(searchText);
}

export function hasAdvancedSearchFilter(advancedSearchGroup: FormGroup): boolean {
    if (isNullOrUndefined(advancedSearchGroup)) {
        return false;
    }

    const controlNames = Object.keys(advancedSearchGroup.controls);
    return controlNames.some(currentName => {
        const currentValue = advancedSearchGroup.controls[currentName].value;
        return !isNullOrUndefined(currentValue) && currentValue !== '';
    });
}

export function anyNullOrUndefined(...values: any[]): boolean {
    return values.some(val => isNullOrUndefined(val));
}
