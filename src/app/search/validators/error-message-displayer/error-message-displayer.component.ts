import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { Subscription } from 'rxjs/internal/Subscription';
import { validateNotEmpty } from 'src/app/util/decorators/validation/validators';
import { getHighestPriorityErrorMessage } from '../validation-errors';
import { GROW_IN_SHRINK_OUT } from 'src/app/util/animations/animations';

@Component({
    selector: 'app-error-message-displayer',
    templateUrl: './error-message-displayer.component.html',
    styleUrls: ['./error-message-displayer.component.scss'],
    animations: [GROW_IN_SHRINK_OUT]
})
export class ErrorMessageDisplayerComponent implements OnInit, OnDestroy {

    @Input()
    public control: AbstractControl;
    private subscription: Subscription;

    public hasErrorMessage: boolean;
    public errorMessage: string;

    ngOnInit(): void {
        if (isNullOrUndefined(this.control)) {
            throw new Error('You must provide a control to the ErrorMessageDisplayerComponent!');
        }

        this.subscription = this.control.statusChanges.subscribe(this.handleControlStatusChange.bind(this));
        this.handleControlStatusChange();
    }

    private handleControlStatusChange(): void {
        const message = getHighestPriorityErrorMessage(this.control.errors);
        this.errorMessage = message;
        this.hasErrorMessage = validateNotEmpty(this.errorMessage);
    }

    public ngOnDestroy(): void {
        if (!isNullOrUndefined(this.subscription)) {
            this.subscription.unsubscribe();
        }
    }

}
