# Github repository browser

A little application that lets you search through github repositories using different search criteras. It is built with **Anguar 9**, using **NGXS** for state management and **PrimeNG** for pre-built components.

## Installation
### NodeJS
To compile and run an angular application, you need to have **NodeJS** installed on your computer. I encountered two ways of installing it:

 - Follow the instruction on the official NodeJS webpage: [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

 - You can use **node version manager** which is available on github: [https://github.com/nvm-sh/nvm](https://github.com/nvm-sh/nvm)
 This application lets you easily install any node version that you want or quickly switch between node versions on your system. There is a video on youtube to help with the installation:
 [https://www.youtube.com/watch?v=JmWiaMgLmt4](https://www.youtube.com/watch?v=JmWiaMgLmt4)

Once you installed NodeJS, the command **node --version** should work and give you the version of NodeJS that is installed on your system. Also, by installing node, you get **npm**, the **n**ode **p**ackage **m**anager.
I used the NodeJS version **v12.16.3** to build this application.

### Angular CLI
After you installed NodeJS, the next is **AngularCLI**. You can follow the instructions on the official website: [https://cli.angular.io/](https://cli.angular.io/)
Once it's installed, the command **ng --version** should work and give you a version number.

### Git
To clone this repository into your local system, you need **git** to be installed on your machine. You can follow the instructions on their website:
[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

To clone the repository using command line, navigate into the destination folder and execute the following command:
`git clone https://kovtamas1991@bitbucket.org/kovtamas1991/github-repository-browser.git`

## Starting the application
Using command line, navigate into the `github-repository-browser` folder and execute the `npm install` command. After the command runs, a node_modules folder should appear in the root directory.
Then execute **one** of the following commands:

- `npm start`

- `npm run start-prod`

Both commands should result in starting up the application, but the first starts it up in **development mode** while the seconds starts it up in **production mode**. 

Once the code is compiled, open up your web browser (my personal suggestion is google chrome) and go the page: [http://localhost:4200/](http://localhost:4200/)

There, the application should await you, up and running.


When running in development mode, and using chrome, I recommend using the **Redux DevTools** extension of the browser.
[https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd)

## Testing
To run the tests, execute the following command: `ng test`.
A new browser window should open up, showing the test results.